define(function(){
    let init = {};
    let {assetRequest,getRestApiService} = require('./httpServices.js');
	init.req_postData = async function(url,opts,error){
		return new Promise(function(resolve,rejected){
	        var pp = {
	            // static define
	        }
	        pp = Object.assign(pp,opts);
	        getRestApiService({
	            'Content-Type' : 'application/json',
	            'Accept' : 'application/json',
	            'X-CSRF-Token' : $('meta[name=_csrf]').attr('content'),
	        },JSON.stringify(pp),url,function(data){
	            try {
	                var data = JSON.parse(data);
	                resolve(data);
	            }
	            catch(e) {error(e)}
	        })
	    })
	}
	init.req_upload = async function(opts,error){
		return new Promise(function(resolved,rejected){
			var formData = new FormData();
			for (var k in opts){
		        formData.append(k,opts[k]);
		    }
			getRestApiService({
				// kalo idupin multipart, bagian Content-Type , Accept nya di kosongin aja
				'X-CSRF-Token' : $('meta[name=_csrf]').attr('content'),
			},formData,assetRequest.upload,function(data){
				data = JSON.parse(data);
				switch(data.status){
					case 'success':
						resolved(data.message);
					break;
					case 'error':
					case 'rejected':
						error(data.message);
					break;
				}
			})
		})
	}
	return init;
})