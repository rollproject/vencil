define(function(){
	let formnya = null;
	const Validator = require('validatorjs');
	let {req_general,req_upload} = require('../helpers/publicFunctions.js');
	let init = {};
	// example upload image basic
	init.testUploadView = function(tagName){
		riot.tag(tagName,'',init.testUploadController);
		return riot.mount(tagName,{});
	}
	init.testUploadController = function(opts){
		let vm = this;
		vm.register = {};
		vm.handleUpload = async function(e){
			e.preventUpdate = true;
			e.preventDefault();
			let data = await req_upload({
				imageUpload : formnya.find('[name=image_upload]')[0]['files'][0],
				nama:formnya.find('[name=nama]').val(),
				_csrf : formnya.find('[name=_csrf]').val()
			},function(err){
				console.log(err);
			})
			console.log(data);
		}
		vm.on('mount',function(){
			formnya = $('[data-is='+opts.dataIs+']');
		})
	}
	// example upload image using croppit
	init.testCroppitUploadView = function(tagName){

	}
	return init;
})