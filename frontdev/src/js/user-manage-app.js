'use strict';
require('../tags/user-manage-app.tag');
let init = {};
let formnya = null;
let {req_general,req_upload} = require('../helpers/publicFunctions.js');
init.uploadView = function(id){
	return riot.mount(id,'user-upload-app',{
		init : init.uploadViewController
	});
}
init.uploadViewController = function(vm){
	vm.imgSource = vm.opts.src_img;
	vm.handleUpload = function(e){
		e.preventUpdate = true;
		formnya.find('#image_upload').trigger('click');
	}
	vm.on('mount',function(){
		formnya = $(vm.refs.root);
		formnya.find('#image_upload').on('change',async function(e){
		    if($(this).val()!=''){
		    	let data = await req_upload({
		    		action: 'image-profile',
					imageUpload : formnya.find('[name=image_upload]')[0]['files'][0],
					_csrf : $('meta[name=_csrf]').attr("content")
				},function(err){
					console.log(err);
				})
				vm.setState({
					imgSource : vm.baseURL+'/api/file?filename='+data.image_profile+'&mode=thumbnail'
				},()=>{
					alert('clear');
				})
		    }
		})
	})
}
init.uploadHeaderView = function(tagName){
	riot.tag(tagName,false,init.uploadHeaderController);
	return riot.mount(tagName);
}
init.uploadHeaderController = function(opts){

}


module.exports = init;