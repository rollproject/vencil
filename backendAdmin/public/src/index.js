							require('require-ensure');
const riot                = require('riot');
const globalMixin         = require('./helpers/riotMixins.js');
							require('./helpers/jqueryPlugins.js');
riot.mixin(globalMixin);
window.riot = riot;
window.runningTag = [];
function go(callback){
    document.addEventListener("DOMContentLoaded", function(event) {
       callback(); 
    });
}
runningTag['user-auth-app'] = function(opts){
    require.ensure([],function(require){
        let jj = require('./js/user-auth-app.js');
        jj = new (jj[opts.functionName])(opts.tagName);
    })
}