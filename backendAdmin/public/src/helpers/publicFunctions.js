define(function(){
    let init = {};
	let weetql = require('./weetql.js');
	init.req_general = async function(opts,error){
		return new Promise(function(resolved,rejected){
			let wq = new (weetql)();
			wq.init(opts.target,opts.request);
			for (var k in opts.fields){
			    wq.setField(k,opts.fields[k]);
            }
            if(opts.whereFilter != null){
            	wq.setWhereFilter(opts.whereFilter);
            }
            if(opts.override != null){
            	wq.setOverrideProcess(opts.override);
            }
			wq.get();
			wq.executing({
				_csrf : $('[name=_csrf]').val()
			},function(data){
				try{
					data = JSON.parse(data);
					resolved(data);
				}catch(ex){
					error(ex);
				}
			})
		})
	}
	return init;
})