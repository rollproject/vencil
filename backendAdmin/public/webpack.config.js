const path              = require('path');
const webpack           = require('webpack');
const htmlPlugin        = require('html-webpack-plugin');
const dashboardPlugin   = require('webpack-dashboard/plugin');
const autoprefixer      = require('autoprefixer'); 
const configuration     = new (require('./configuration.js'))();
var CopyWebpackPlugin   = require('copy-webpack-plugin');

const PATHS = {
  app: path.join(__dirname, 'src'),
  images:path.join(__dirname,'src/assets/'),
  build: path.join(__dirname, 'dist')
};
module.exports = {
  entry: {
    app: PATHS.app
  },
  output: {
    path: PATHS.build,
    // filename: 'bundle.[hash].js'
    filename: configuration.mode_option.debug==true?'bundle.js':'bundle.[hash].js',
    chunkFilename:configuration.mode_option.debug==true?'[name].chunk.js':'[name].[hash].chunk.js',
    publicPath: configuration.mode_option.debug == true ? configuration.path_source.DEBUG_URL : configuration.path_source.DIST_URL
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(bower_components)/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: false,
          presets: []
        }
      },
      {
        test: /\.(tag)?$/,
        loader: "tag-loader",
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(css|less|scss|sass)?$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "sass-loader" // compiles Sass to CSS
        }]
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.(ico|jpg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,        
        loader: 'file-loader',
        query: {
          name: '[path][name].[ext]'
        }
      },      
    ],
    
  },
  externals : {
    lodash : {
      commonjs: "lodash",
      amd: "lodash",
      root: "_" // indicates global variable
    },
    jquery:{
      root: "$" // indicates global variable
    }
  },

  plugins:[
    new dashboardPlugin(),
    new webpack.ProvidePlugin({
        // $: "jquery",
        // jQuery: "jquery"
    }),
    /*
    new webpack.HotModuleReplacementPlugin({
        multiStep: true
    }),*/
    /*
    new htmlPlugin({
      template:path.join(PATHS.app,'index.html'),
      inject:'body'
    }),*/
    new CopyWebpackPlugin([
            // { from: 'src/assets', to: 'src/assets' },
            // { from: 'ico', to: 'ico' },
            // { from: 'img', to: 'img' },
            // { from: 'font-awesome', to: 'font-awesome' },
            // { from: 'js', to: 'js' },
        ])
  ]
};