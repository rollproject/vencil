var express 			= require('express');
var router 				= express.Router();
var User			= require('../model/user.js');
var checkAuthUser = function(req,res,next){
	if(req.session.user_id){
		next();
	}else{
		res.redirect('/signin');
	}
}
var hasAuth = function(req,res,next){
	if(req.session.user_id){
		res.redirect('/');
	}else{
		next();	
	}
}

router.get('/signout',function(req,res,next){
	req.session.destroy(function(err){  
        if(err){  
            console.log(err);  
        }  
        res.redirect('/signin'); 
    });  
})
router.get('/signin',hasAuth,function(req,res,next){
	res.render('user-signin',{
		title : 'Vencil Admin Login',
	})
})
router.get('/setting',hasAuth,function(req,res,next){
	res.render('user-setting',{
		title : 'Vencil Admin Setting',
	})
})
router.get('/signup',hasAuth,function(req,res,next){
	res.render('user-signup',{
		title : 'Vencil Admin Register',
	})
})
router.get('/:username',checkAuthUser,function(req,res,next){
	let userMember = new (User)();
	userMember.findOne({
		username : req.params.username
	}).exec(function(err,data){
		if(err) res.status(500).send({
			status : 'error',
			message : err.message
		})
		if(data != null){
			res.render('user-profile',{
				title : 'Vencil Admin Register',
				data : data,
			});
		}else{
			res.render('user-not-found',{
				title : 'Vencil Admin Not Found',
			});
		}
	})
})
router.get('/',checkAuthUser,function(req,res,next){
	res.render('home',{
		title : 'Vencil'
    })
})
module.exports = router;