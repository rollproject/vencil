const express 					= require('express');
const router 					= express.Router();
const {
		weetQl,
		getRequireFilter,
		getMiddleware
	} 							= require('../classes/weetqlMongoose.js');
const bcrypt 					= require('bcrypt');
const saltRounds 				= 10;
const Validator 				= require('validatorjs');
const local_user 				= require('../passportjs/localUser.js');
const _ 						= require('lodash');

router.post('/weetql', function (req, res, next) {
  	weetQl(res,req.body.restQl,weetQlFunction(req,res,next));
});
let weetQlFunction = function(req,res,next){
	let vm = this;
	// define model remember case sensitive
	vm.User = require('../model/user');
	vm.testDriveFunction = function(q,obj){
		let result = getRequireFilter(obj,['testDriveChildren']);
	    if(result.rejected()){
	        return res.status(500).send({
	        	message : result.message()
	        });
	    }
	}
	vm.signinMemberProcess = function(q,obj){
		local_user.signIn(function(username,password,done){
			process.nextTick(function () {
				let setObjectUserLogin = {
					username : username,
					password : password
				}
				let setLogin = {};
				let validation = new Validator(setObjectUserLogin, {
					username: 'required|email'
				});
				if(validation.fails()){
					setLogin.username = obj.fieldCheck.username;
				}else{
					setLogin.email = obj.fieldCheck.username;
				}
				let userEmail = new (vm.User)();
				validation = new Validator(setObjectUserLogin, {
					password: 'required|min:6'
				}); 
				if(validation.fails()){
					return res.status(500).send({
						status : 'error',
						message : validation.errors
					})
				}
				userEmail.findOne(setLogin).exec(function(err,data){
					let gg = data;
					if(err) return res.status(500).send(err.message);
					if(data == null) return res.status(500).send({
							status : 'rejected',
							message : 'Wrong Username or password'
						})
					if(bcrypt.compareSync(setObjectUserLogin.password, data.password)){
						let oo = _.omit(JSON.parse(JSON.stringify(gg)),"password")
						// return done(null,oo);
						req.session.user_id = oo._id;
						req.session.save(function(){
							res.header('Access-Control-Allow-Credentials', 'true');
							var fullUrl = req.protocol + '://' + req.get('host') + '/';
							return res.status(200).send({
								status : 'success',
								message : 'Login Succesfully!',
								foward : fullUrl
							})
						});
            			
					}else{
						return res.status(500).send({
							status : 'rejected',
							message : 'Wrong Username or password'
						})
					}
				});
			});
		})
		req.body.username = obj.fieldCheck.username;
		req.body.password = obj.fieldCheck.password;
		local_user.passport.authenticate('local', {
	    // kalo pake ajax ini enggak ngaruh di ajax
	    // successRedirect: '/login/register',
	    // failureRedirect: '/login',
	    // failureFlash: true
  		})(req, res, next)
	}
	vm.newRegisterMember = async function(q,obj){
		let result = await getMiddleware(obj,[vm.checkEmailMemberExistMiddleware,vm.checkMemberUsernameExistMiddleware],function(err){
			// missing functtion
			if(err==true) return res.status(500).send({
				status : 'error',
				message : 'Any function is missing!'
			})
			// from middleware
			return res.status(500).send(err);
		});
		var validation = new Validator(obj.fieldCheck, {
			username: 'required|min:4',
		    password: 'required|min:6',
		    email: 'required|email',
		    full_name: 'required'
		});
		if(validation.fails()){
			return res.status(500).send({
				status : 'error',
				message : validation.errors
			})
		}
		bcrypt.genSalt(saltRounds, function(err, salt) {
			bcrypt.hash(obj.fieldCheck.password, salt, function(erCript, hash) {
				if(erCript) return res.status(500).send({
					status : 'error',
					message : erCript.message
				})
			  	// Store hash in your password DB.
			  	obj.fieldCheck.password = hash;
			  	obj.fieldCheck.status = 1;
			  	obj.fieldCheck.is_banned = false;
			  	q.create(obj.fieldCheck,function(err,data){
					if(err) return res.status(500).send({
						status : 'error',
						message : err.message
					})
					var fullUrl = req.protocol + '://' + req.get('host') + '/signin';
					res.status(200).send({
						status : 'success',
						message : 'success register',
						foward : fullUrl
					})
				})
			});
		})
	}
	
	// fetch
	vm.fetchAdmins = function(q,obj){
		return q;
	}
	// middleware
	vm.checkEmailMemberExistMiddleware = function(result,opts,next,error){
		let userEmail = new (vm.User)();
		userEmail.findOne()
			.where('email').equals(opts.fieldCheck.email)
			.exec(function(err,data){
				let messageError = {
					status : 'error',
					message : 'Email allready exist'
				}
				if(data != null) {
					return error(messageError);
				}
				next(true);
			})
	}
	vm.checkMemberUsernameExistMiddleware = function(result,opts,next,error){
		let userEmail = new (vm.User)();
		userEmail.findOne()
			.where('username').equals(opts.fieldCheck.username)
			.exec(function(err,data){
				let messageError = {
					status : 'error',
					message : 'Username allready exist'
				}
				if(data != null) {
					return error(messageError);
				}
				next(true);
			})
	}
	return vm;
}
module.exports = router;