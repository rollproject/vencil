const express 		    = require('express')
const path 			      = require('path');
const app 		      	= express()
var csrf 			        = require('csurf')
const nunjucks 		    = require('nunjucks');
const weetql		      = require('./routes/weetql.js');
const publicAccess    = require('./routes/publicAccess.js');
const cookieParser 	    = require('cookie-parser');
const bodyParser 		    = require('body-parser');
const session 		      = require('express-session');
const MongoDBStore 	    = require('connect-mongodb-session')(session);
const passport          = require('passport');
const {mongoose,uriString}      = require('./classes/mongooseClass');
const User              = require('./model/user.js');
// session express js
const store = new MongoDBStore(
  {
    uri: uriString,
    collection: 'mySessionsAdmin'
});
// Passport does not directly manage your session, it only uses the session.
// So you configure session attributes (e.g. life of your session) via express
const sessionOpts = {
  saveUninitialized: false, // saved new sessions
  resave: false, // do not automatically write to the session store
  store: store,
  secret: 'avmadkfvk',
  cookie: { 
    secure: false,
    httpOnly: true, 
    maxAge: 2419200000 } // configure when sessions expires
}
nunjucks.configure(['views','views/pages'], {
  autoescape: true,
  express: app,
  watch:true,
  noCache:true
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser());
app.use(csrf({ cookie: true }))
app.use(session(sessionOpts));
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine','html');
// define static public folder
app.use('/static', express.static('public'))
app.use(async function(req,res,next){
  res.locals.csrfToken = req.csrfToken();
  if(req.session.user_id){
    var gg = new (User)();
    gg = await gg.getCurrentUser({
      _id : req.session.user_id
    },function(error){
      if(error) return res.status(500).send({
        status : 'error',
        message : error.message
      })
    })
    res.locals.current_data = gg;
  }else{
    res.locals.is_login = false;
  }
  next();
})
app.use('/api',weetql);
app.use('/',publicAccess);
app.listen(process.env.PORT || 8081, process.env.IP || "0.0.0.0", function(){
  // var addr = app.address();
   console.log("App server listening");
});
