
const _ = require('lodash');
module.exports = {
	weetQl : function(res,data,vm){
		let wq = JSON.parse(data);
		for(var a=0;a<wq.length;a++){
			let gg = wq[a];
			let the_class = gg.model;
			the_class = new (vm[the_class]);
			// get model class
	        switch(gg.mode){
	        	case 'single':
	        		the_class = the_class.findOne();
	        	break;
	        	case 'all':
	        		the_class = the_class.find();
	        	break;
	        	case 'new':
	        	break;
	        }
			if(gg.backendFilter.length > 0){
				let gotError = null;
				for(var aeo=0;aeo<gg.backendFilter.length;aeo++){
					let checkData = vm[backendFilter[aeo]](the_class);
					if(checkData.status == 'error'){
						gotError = checkData;
					}
				}
				if(gotError !=  null){
					return res.status(500).send(gotError);
				}
			}
			if(gg.with.length > 0){
				for(var aeo=0;aeo<gg.with.length;aeo++){
					the_class.populate(gg.with[aeo]);
				}
			}
			// jika menggunakan backend filter maka where tidak di perbolehkan
	        // prioritas di backendFilter
			if(gg.backendFilter.length == 0){
				if(gg.where.length > 0){
					for(var aeo=0;aeo<gg.where.length;aeo++){
						switch(gg.where[aeo].operator){
							case '>':
								the_class.where(gg.where[aeo].field).gt(gg.where[aeo].value);
							break;
							case '<':
								the_class.where(gg.where[aeo].field).lt(gg.where[aeo].value);
							break;
							case '=':
								the_class.where(gg.where[aeo].field).equals(gg.where[aeo].value);
							break;
						}
					}
				}
			}else{
				if(gg.where.length > 0){
					res.status(500).send({
						'status' : 'rejected',
						'message' : 'You cant use backendFilter and where Clause together'
					});
				}
			}
			// masih proses kualifikasi
			if(gg.whereFilter.length > 0){
				let gotError = null;
				try{
					for(var aeo=0;aeo<gg.whereFilter.length;aeo++){
						the_class = vm[gg.whereFilter[aeo]](the_class,gg);
						// ini filter harus mengandalkan validator punya nodejs
						// jika error maka
						if(the_class.fails != null){
							gotError = the_class;
						}
					}
				}catch(error){
					gotError = error.message;
				}
				if(gotError !=  null){
					return res.status(500).send({
						'status' : 'error',
	                    'message': gotError
					})
				}
			}
			// tidak digunakan di mongoose, karena bakal jadi middleware
			if(gg.validator.length > 0){

			}
			// ini untuk override process default jadi full custom code di function
	        // console.log(gg.overrideProcess);
	        if(gg.overrideProcess != null){
	        	return vm[gg.overrideProcess](the_class,gg);
	        }
	        if(gg.backendFilter.length > 0){
	        	for(var aeo=0;aeo<gg.backendFilter.length;aeo++){
	        		the_class = vm[gg.backendFilter[aeo].func](gg.backendFilter[aeo].value);
	        	}
	        }
	        switch(gg.action){
	        	case 'get':
	        		the_class.exec(function(err,data){
	        			if (err) return res.status(500).send({
			        			'status':'error',
			        			'message':err
			        		});
	        			res.status(200).send({
		        			'status':'success',
		        			'message':data
		        		});
	        		})
	        	break;
	        }
		}
	},
	getRequireFilter : function(aa,theFilter){
		let requireFlter = new RequireFilter(aa,theFilter);
		return requireFlter;
	},
	getMiddleware : async function(aa,theFilter,error){
		return new Promise(function(resolve,rejected){
			Middleware(aa,theFilter,function(data){
				resolve(true);
			},error);
		});
	}
}
let RequireFilter = function(aa,arrayFunction){
	this.storeArrayFilter = [];
	this.isReadyBackendFilter = true;
	this.isEquals = false;
	this.saveMissingRequireFilter = '';
	this.init = function(gg,arrayFunction){
		this.storeArrayFilter = arrayFunction;
		if(gg.backendFilter.length>0){
			for(var aeo=0;aeo<gg.backendFilter.length;aeo++){
				for(var eeo=0;eeo<arrayFunction.length;eeo++){
					if(arrayFunction[eeo] == gg.backendFilter[aeo]){
						this.isEquals = true;
						break;
					}else{
						this.saveMissingRequireFilter = arrayFunction[eeo];
					}
				}
				if(this.isEquals == false){
					break;
				}
			}
		}else{
			this.isReadyBackendFilter = false;
		}
	}
	this.rejected = function(){
		if(this.isEquals == false) return true;
	}
	this.message = function(){
		if(this.isReadyBackendFilter){
			return {
				status:'rejected',
				message:'need filter '+this.saveMissingRequireFilter
			}
		}else{
			return {
				status : 'rejected',
				message : 'need filter '+this.storeArrayFilter.join(', ')
			}
		}
	}
	this.init(aa,arrayFunction);
}
let Middleware = function(aa,arrayFunction,finish,error){
	let a=0;
	let val = null;
	let pp = arrayFunction;
	var next = function(data){
		if(arrayFunction.length-1 == a){
			return finish();
		}
		a= a+1;
		if(pp[a] == null){
			return error(true); 
		}
		pp[a](data,aa,next,error);
	}
	pp[a](val,aa,next,error);
}