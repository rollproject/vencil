const {mongoose} = require('../classes/mongooseClass');
const Schema = mongoose.Schema;

let userSchema = new Schema({
  username:  {
    type: String,
    index: true,
    unique: true
  },
  password: String,
  full_name: String,
  email: {
    type: String,
    unique: true
  },
  date: { type: Date, default: Date.now },
  status: Boolean,
  is_banned: Boolean,
},
{timestamps: { createdAt: 'created_at', updatedAt : 'updated_at' }}
);

let UserMember = mongoose.model('user_admin',userSchema);
UserMember.limit = {
      'max_row' : 100,
      'row' : 10,
      'column' : [
          'column1','column2','column3'
      ]
  };
UserMember.getCurrentUser = async function(opts,error){
  return new Promise(function(resolve,rejected){
    let gg = UserMember.findOne(opts).exec(function(err,data){
      if(err) return error(err);
      if(data != null){
        resolve(data);
      }else{
        resolve(null)
      }
    })
  })
}
UserMember.getAll = async function(opts,error){
  return new Promise(function(resolve,rejected){
    let gg = UserMember.find(opts).exec(function(err,data){
      if(err) return error(err);
      if(data != null){
        resolve(data);
      }else{
        resolve([])
      }
    })
  })
}
UserMember.getUserProfile = UserMember.getCurrentUser;
module.exports = function(){
    return UserMember
};

