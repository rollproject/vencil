const userModel = require('../model/user.js');
let init = {}

init.checkEmailMemberExistMiddleware = function(result,opts,next,error){
	let userEmail = new (userModel)();
	userEmail.findOne()
		.where('email').equals(opts.email)
		.exec(function(err,data){
			let messageError = {
				status : 'error',
				message : 'Email allready exist'
			}
			if(data != null) {
				return error(messageError);
			}
			next(true);
		})
}

init.checkMemberUsernameExistMiddleware = function(result,opts,next,error){
	let userEmail = new (userModel)();
	userEmail.findOne()
		.where('username').equals(opts.username)
		.exec(function(err,data){
			let messageError = {
				status : 'error',
				message : 'Username allready exist'
			}
			if(data != null) {
				return error(messageError);
			}
			next(true);
		})
}

module.exports = {
    ...init
}