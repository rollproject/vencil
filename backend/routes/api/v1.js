const {
    user,
    auth
} = require('../../controllers/v1');
var checkAuthUser = function(req,res,next){
	if(req.session.user_id){
		next();
	}else{
		res.redirect('/signin');
	}
}
let initRoute = function(router){
	// auth
	router.get('/auth',checkAuthUser,auth.getCurrentUser);
	router.get('/auth/session',auth.getSession);
	router.get('/auth/csrf',auth.getCsrf);
	router.post('/auth',auth.signin);
	router.post('/auth/signup',auth.signup);
	// user
	/*
	router.get('/user/all',user.getAllUser);
	router.get('/user',user.getCurrentInfo);
	router.get('/user/follower',user.getCurrentFollower);
	router.get('/user/:id/follower',user.getUserFollower);
	router.post('/user/:id/follower',user.addNewFollow);*/
	return router;
}

module.exports = function(router){
	return initRoute(router);
};