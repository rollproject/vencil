const fs 			= require('fs');
const sharp			= require('sharp');

module.exports = function(targetSaveDisk){
	let vm = this;
	vm.target_disk = '../storages/uploads';
	vm.save_folder = '';
	vm.the_file_path_image = '';
	vm.the_file_name = '';
	vm.the_file_type = 'png';
	vm.init = function(theTargetDisk){
		if(theTargetDisk != null){
			vm.target_disk = theTargetDisk;
		}
	}
	vm.setSaveImage = function(originalSource,next){
		console.log(originalSource.path);
		vm.the_file_path_image = originalSource.path;
		vm.the_file_name = originalSource.originalFilename;
		// vm.the_file_type = fileType;
		next();
	}
	vm.getSaveFolder = function(){
		return vm.save_folder;
	}
	vm.getFileImage = function(){
		return vm.the_file_path_image;
	}
	vm.getDisk = function(){
		return vm.target_disk;
	}
	vm.delete = function(name){
		// Storage::delete($name);
	}
	vm.fetchImage = async function(fullSource,error){
		return new Promise(function(resolve,rejected){
			fs.readFile(fullSource, function(err, data) {
			  if (err) return error(err);
			  resolve(data);
			});
		})
	}
	vm.save = async function(name,width,height,error){
		return new Promise(function(resolve,rejected){
			let saveName = vm.the_file_name;
			let fileContent = null;
			if(name != null){
				saveName = name;
			}
			if(width != null){
				fileContent = sharp(vm.the_file_path_image);
				fileContent
				  .metadata({
				  	width : width,
				  	format : vm.the_file_type
				  })
				  .then(function(metadata) {
				    return fileContent
				      .resize(width,null)
				      .webp()
				      .toFile(vm.target_disk+'/'+saveName,function(err){
					      	if (err) return error(err);
						  	console.log('File Generated');
						  	resolve();
				      })
				  })
				  
				return;
			}
			if(height != null){
				fileContent = sharp(vm.the_file_path_image);
				fileContent
					.metadata({
						height : height,
						format : vm.the_file_type
					})
					.then(function(metadata) {
						return fileContent
							.resize(null,height)
							.webp()
							.toFile(vm.target_disk+'/'+saveName,function(err){
								if (err) return error(err);
								console.log('File Generated');
								resolve();
							})
				  		})
				return;
			}
			fileContent = sharp(vm.the_file_path_image);
			fileContent
				.metadata({
					format : vm.the_file_type
				})
				.then(function(metadata) {
				return fileContent
					.webp()
					.toFile(vm.target_disk+'/'+saveName,function(err){
						if (err) return error(err);
						console.log('File Generated');
						resolve();
					})
		  		})
		})
	}
	vm.init(targetSaveDisk)
}