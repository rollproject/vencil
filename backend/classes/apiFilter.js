const _ = require('lodash');
module.exports = {
	weetQl : function(res,data,vm){
		if(data.action != null){
        	return vm[data.action](data);
        }else{
        	return res.status(403).send({
        		'status' : 'error',
        		'message' : 'Your action is not found'
        	})
        }
	},
	getRequireFilter : function(aa,theFilter){
		let requireFlter = new RequireFilter(aa,theFilter);
		return requireFlter;
	},
	getMiddleware : async function(aa,theFilter,error){
		return new Promise(function(resolve,rejected){
			Middleware(aa,theFilter,function(data){
				resolve(true);
			},error);
		});
	}
}
let RequireFilter = function(aa,arrayFunction){
	this.storeArrayFilter = [];
	this.isReadyBackendFilter = true;
	this.isEquals = false;
	this.saveMissingRequireFilter = '';
	this.init = function(gg,arrayFunction){
		this.storeArrayFilter = arrayFunction;
		if(gg.backendFilter.length>0){
			for(var aeo=0;aeo<gg.backendFilter.length;aeo++){
				for(var eeo=0;eeo<arrayFunction.length;eeo++){
					if(arrayFunction[eeo] == gg.backendFilter[aeo]){
						this.isEquals = true;
						break;
					}else{
						this.saveMissingRequireFilter = arrayFunction[eeo];
					}
				}
				if(this.isEquals == false){
					break;
				}
			}
		}else{
			this.isReadyBackendFilter = false;
		}
	}
	this.rejected = function(){
		if(this.isEquals == false) return true;
	}
	this.message = function(){
		if(this.isReadyBackendFilter){
			return {
				status:'rejected',
				message:'need filter '+this.saveMissingRequireFilter
			}
		}else{
			return {
				status : 'rejected',
				message : 'need filter '+this.storeArrayFilter.join(', ')
			}
		}
	}
	this.init(aa,arrayFunction);
}
let Middleware = function(aa,arrayFunction,finish,error){
	let a=0;
	let val = null;
	let pp = arrayFunction;
	var next = function(data){
		if(arrayFunction.length-1 == a){
			return finish();
		}
		a= a+1;
		if(pp[a] == null){
			return error(true); 
		}
		pp[a](data,aa,next,error);
	}
	pp[a](val,aa,next,error);
}