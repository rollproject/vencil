const auth = require('./auth.js');
const user = require('./user.js');
module.exports = {
    auth,
    user
}