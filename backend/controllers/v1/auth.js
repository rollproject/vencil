const fs 						= require('fs-extra');
const path 			      		= require('path');
const multiparty 				= require('multiparty');
const bcrypt 					= require('bcrypt');
const saltRounds 				= 10;
const Validator 				= require('validatorjs');
const local_user 				= require('../../passportjs/localUser.js');
const _ 						= require('lodash');
const {weetQl,getMiddleware}	= require('../../classes/apiFilter.js');
const userModel = require('../../model/user.js');
const {
    checkEmailMemberExistMiddleware,
    checkMemberUsernameExistMiddleware
} = require('../../middleware');
let init = {};
init.signin = function(req,res,next){
    local_user.signIn(function(username,password,done){
		process.nextTick(function () {
			let setObjectUserLogin = {
				username : username,
				password : password
			}
			let setLogin = {};
			let validation = new Validator(setObjectUserLogin, {
				username: 'required|email'
			});
			if(validation.fails()){
				setLogin.username = username;
			}else{
				setLogin.email = username;
			}
			let userEmail = new (userModel)();
			validation = new Validator(setObjectUserLogin, {
				password: 'required|min:6'
			}); 
			if(validation.fails()){
				return res.status(500).send({
					status : 'error',
					message : validation.errors
				})
			}
			userEmail.findOne(setLogin).exec(function(err,data){
				let gg = data;
				if(err) return res.status(500).send(err.message);
				if(data == null) return res.status(500).send({
						status : 'rejected',
						message : 'Wrong Username or password'
					})
				if(bcrypt.compareSync(setObjectUserLogin.password, data.password)){
					let oo = _.omit(JSON.parse(JSON.stringify(gg)),"password")
					// return done(null,oo);
					console.log(oo);
					req.session.user_id = oo._id;
					req.session.save(function(){
						res.header('Access-Control-Allow-Credentials', 'true');
						var fullUrl = req.protocol + '://' + req.get('host') + '/'+data.username;
						return res.status(200).send({
							status : 'success',
							message : 'Login Succesfully!',
							foward : fullUrl
						})
					});
        			
				}else{
					return res.status(500).send({
						status : 'rejected',
						message : 'Wrong Username or password'
					})
				}
			});
		});
	})
	local_user.passport.authenticate('local', {
    // kalo pake ajax ini enggak ngaruh di ajax
    // successRedirect: '/login/register',
    // failureRedirect: '/login',
    // failureFlash: true
  	})(req, res, next)
}
init.signup = async function(req,res,next){
    let result = await getMiddleware(req.body,[checkEmailMemberExistMiddleware,checkMemberUsernameExistMiddleware],function(err){
		// missing functtion
		if(err==true) return res.status(500).send({
			status : 'error',
			message : 'Any function is missing!'
		})
		// from middleware
		return res.status(500).send(err);
	});
	var validation = new Validator(req.body, {
		username: 'required|min:4',
	    password: 'required|min:6',
	    email: 'required|email',
	    full_name: 'required'
	});
	if(validation.fails()){
		return res.status(500).send({
			status : 'error',
			message : validation.errors
		})
	}
	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(req.body.password, salt, function(erCript, hash) {
			if(erCript) return res.status(500).send({
				status : 'error',
				message : erCript.message
			})
		  	// Store hash in your password DB.
		  	req.body.password = hash;
		  	req.body.status = 1;
		  	req.body.is_banned = false;
		  	let usernya = new (userModel)();
		  	usernya.create(req.body,function(err,data){
				if(err) return res.status(500).send({
					status : 'error',
					message : err.message
				})
				var fullUrl = req.protocol + '://' + req.get('host') + '/signin';
				res.status(200).send({
					status : 'success',
					message : 'success register',
					foward : fullUrl
				})
			})
		});
	})
}

init.getCsrf = function(req,res){
    res.status(200).send({
        csrf : req.csrfToken()
    })
}

init.getSession = function(req,res){
    
}

init.getCurrentUser = function(req,res){
    let us = new (userModel)();
    us.findOne({
        _id : req.session.user_id
    },function(err,data){
        res.status(200).send(data);
    });
}

module.exports = init;